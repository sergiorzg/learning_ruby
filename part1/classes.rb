# encoding: UTF-8
#
# Defining classes
#
# Author::    github.com/rozasdev (mailto:rozasdev at gmail.com)
# License::   Apache License 2.0

################################
# MySuperClass
# Example superclass
#
class MySuperClass

  # Constructor
  def initialize(param1,param2)
    @attr1 = param1
    @attr2 = param2
  end

  # Getter for attr1
  def attr1
    @attr1
  end

  # Getter for attr2
  def attr2
    @attr2
  end

  # Setter for attr1
  def attr1=(new_value)
    @attr1 = new_value
  end

  # Setter for attr2
  def attr2=(new_value)
    @attr2 = new_value
  end

end

################################
# MyClass
# Contains @attrX (1..6)
#
class MyClass < MySuperClass

  def initialize()
  end

  # Automatic getters & setter for @attr3 and @attr4
  attr_accessor :attr3, :attr4

  # Automatic getter for attr5
  attr_reader :attr5

  # Automatic setter for att6
  attr_writer :attr6

  # Overriding to_s method
  def to_s
    return "#{@attr1}, #{@attr3}, #{@attr5}"
  end

end

################################
# MySecondClass
# Contains class attributes, methods & constants
#
class MySecondClass

  # Class constant
  MY_CLASS_CONSTANTS = ["Value 1", "Value2"]

  # Class attribute accessed through a class method.
  def MySecondClass.attribute
    @@attribute = "I'm a class attribute"
  end

  # Class method
  def MySecondClass.my_class_method
    puts "This is a class method"
  end

end

################################
# main
# Tests the previously defind classes.
#
def main
  my = MyClass.new
  my.attr6 = "This is attribute #6"
  puts "MyClass::attr5 (no value given) = #{my.attr5}"
  my.attr3 = "This is attribute #3"
  puts "MyClass::attr3 = #{my.attr3}"
  puts "MySecondClass::attribute = #{MySecondClass.attribute}"
end

# Call main function
main
