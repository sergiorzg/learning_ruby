# encoding: UTF-8
#
# Modules examples
#
# Author::    github.com/rozasdev (mailto:rozasdev at gmail.com)
# License::   Apache License 2.0

require 'awesome_print'

#######################################
# Stackable
# Module that defines the basic behavior of an array based stack.
#
module Stackable

  # Getter for the internal implementation of the stack
  def stack
    @stack ||= []
  end

  # Puts an item at the top of the stack.
  def put(item)
    stack.push(item)
  end

  # Gets the item from the top of the stack.
  def take
    stack.pop
  end
end

#######################################
# MyStack
# Stack class that is Stackable
#
class MyStack
  include Stackable
end

#######################################
# Suitcase
# Stub for example purposes
#
class Suitcase
end

#######################################
# CargoHold
# Class that manages the insertion and removal of
# suitacases in an airplane.
#
class CargoHold

  # Use stackable module
  include Stackable

  # Loads a new suitcase into the airplane
  def load(suitcase)
    puts "Loading suitcase \##{suitcase.object_id} to airplane"
    self.put(suitcase)
  end

  def unload
    puts "Unloading suitcase \##{self.take.object_id} from airplane"
  end
end

################################
# main
# Tests the previously defined classes.
#
def main
  my_stack = MyStack.new
  my_stack.put('#1')
  my_stack.put('#2')
  my_stack.put('#3')

  puts "Stack should contain 3 elements"
  ap my_stack.stack

  my_stack.take
  puts "Now just 2"
  ap my_stack.stack

  my_stack.put('#3')
  my_stack.put('#4')
  my_stack.put('#5')
  puts "And now 5!"
  ap my_stack.stack

  puts
  puts

  puts "Airplane Suitcase handler example"
  cargo = CargoHold.new
  suitcase_1 = Suitcase.new
  suitcase_2 = Suitcase.new
  suitcase_3 = Suitcase.new
  print "\t-"
  cargo.load(suitcase_1)
  print "\t-"
  cargo.load(suitcase_2)
  print "\t-"
  cargo.load(suitcase_3)
  print "\t-"
  cargo.unload
end

# Call to main function
main
