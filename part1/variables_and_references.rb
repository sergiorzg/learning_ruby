# encoding: UTF-8
#
# Handling variables and references.
#
# Author::    github.com/rozasdev (mailto:rozasdev at gmail.com)
# License::   Apache License 2.0

$ORIGINAL_TEXT = "I'm the original one"
$MODIFIED_TEXT = "I'm the original one, but modified"
$NEW_TEXT = "I'm the original one, but with this new content."

#########################################
# check_with_literals
# Performs checkings printing literal strings.
#
def check_with_literals
  original_string = String.new.replace($ORIGINAL_TEXT)
  copy_from_original = original_string.clone
  reference_to_original = original_string
  original_string.replace($MODIFIED_TEXT)

  puts "Checking equality with literals ..."
  puts "\tThe original String is \"#{original_string}\" and should be equals to \"#{$MODIFIED_TEXT}\""
  puts "\tThe reference String is \"#{reference_to_original}\" and should be equals to \"#{original_string}\""
  puts "\tThe copy String is \"#{copy_from_original}\" and should be equals to \"#{$ORIGINAL_TEXT}\""

  original_string = String.new.replace($NEW_TEXT)
  puts "\tThe original String is \"#{original_string}\" and should be equals to \"#{$NEW_TEXT}\""
  puts "\tThe reference String is \"#{reference_to_original}\" and should be equals to \"#{$MODIFIED_TEXT}\""
end

#########################################
# check_with_equals
# Performs checkings printing booleans
#
def check_with_equals
  original_string = String.new.replace($ORIGINAL_TEXT)
  copy_from_original = original_string.clone
  reference_to_original = original_string
  original_string.replace($MODIFIED_TEXT)

  puts "Checking equality with .eql? ..."
  puts "\tThis should be true: " + original_string.eql?($MODIFIED_TEXT).to_s
  puts "\tThis should be true: " + reference_to_original.eql?(original_string).to_s
  puts "\tThis should not: " + reference_to_original.eql?($ORIGINAL_TEXT).to_s
  puts "\tThis should be true: " + copy_from_original.eql?($ORIGINAL_TEXT).to_s

  original_string = String.new.replace($NEW_TEXT)
  puts "\tThis should be true: " + original_string.eql?($NEW_TEXT).to_s
  puts "\tThis should be true: " + reference_to_original.eql?($MODIFIED_TEXT).to_s
  puts "\tThis should not either: " + reference_to_original.eql?($NEW_TEXT).to_s

end

#########################################
# check_frozen_strings
# Performs checkings with a frozen string.
#
def frozen_strings
  original_string = String.new.replace($ORIGINAL_TEXT)
  original_string.freeze
  duplicated_frozen_string = original_string.dup

  puts "Testing frozen strings..."
  begin
    duplicated_frozen_string.replace($ORIGINAL_TEXT)
    puts "\tThis text should appear since we've modified a duplicated string from a frozen one"
  rescue Exception => e
    puts "\tThis text should NOT appear :( (exception #{e.message})"
  end

  cloned_frozen_string = original_string.clone
  begin
    cloned_frozen_string.replace($ORIGINAL_TEXT)
    puts "\tThis text should NOT appear :("
  rescue Exception => e
    puts "\tThis text should appear since we've tried to modify a cloned string from a frozen one (exception #{e.message})"
  end
end

#########################################
# main
# Calls the different functions.
#
def main
  check_with_literals
  check_with_equals
  frozen_strings
end

# Main call
main
