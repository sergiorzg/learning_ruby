# encoding: UTF-8
#
# Celsius <=> Farenheit degrees converter.
#
# Author::    github.com/rozasdev (mailto:rozasdev at gmail.com)
# License::   Apache License 2.0

###########################################################
# Takes some floating point number as Celsius temperature
# and returns its Farenheit counterpart.
def celsius_to_farenheit(celsius)
  return (celsius * 9 / 5) + 32
end

###########################################################
# Takes some floating point number as Farenheit temperature
# and returns its Celsius counterpart.
#
def farenheit_to_celsius(farenheit)
  return (farenheit - 32) * 5 / 9
end

###########################################################
# main
# Main function
def main
  # Insufficient arguments
  if ARGV.length != 2
    print_usage
    exit (-1)
  # Case for calling appropriate methods.
  else
    case ARGV[0].to_i
      when 1
        puts "#{ARGV[1].to_i} Celsius are #{celsius_to_farenheit(ARGV[1].to_i)} Farenheits"
      when 2
        puts "#{ARGV[1].to_i} Farenheits are #{farenheit_to_celsius(ARGV[1].to_i)} Celsius"
      else
        print_usage
        exit (-1)
    end
  end
end

########################
# print_usage.
# Prints the usage info.
#
def print_usage
    puts "USAGE: temperature_converter <operation> <value>\n"
    puts "\toperation: 1 for Celsius to Farenheit, 2 for Farenheit to Celsius"
    puts "\tvalue: The temperature value to be converted"
end

# Call main function
main
