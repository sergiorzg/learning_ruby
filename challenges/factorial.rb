# encoding: UTF-8
#
# Factorial
#
# Author::    github.com/rozasdev (mailto:rozasdev at gmail.com)
# License::   Apache License 2.0

# Gets the factorial for a number
def factorial(num)
  if num == 0
    return 1
  else
    return factorial(num-1) * num
  end
end

# Just call the factorial function
puts factorial(18)