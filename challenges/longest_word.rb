# encoding: UTF-8
#
# Longest Word
#
# Author::    github.com/rozasdev (mailto:rozasdev at gmail.com)
# License::   Apache License 2.0

# Gets the longest word in an array
def longest_word(arr)
  arr.sort_by! { |x| x.length }
  arr.last
end

# Just call the function
words = %w{Several Rhino Charming Enlighten}
puts longest_word(words)