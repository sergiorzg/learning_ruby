# encoding: UTF-8
#
# Simple adding: Add up all the numbers from 1 to num.
#
# Author::    github.com/rozasdev (mailto:rozasdev at gmail.com)
# License::   Apache License 2.0

# Implements the adding algorithm.
def simple_adding(num)
  accum = 0
  while num > 0
    accum += num
    num -= 1
  end
  accum
end

# Just call the function.
puts simple_adding(12)
