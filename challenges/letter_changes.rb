# encoding: UTF-8
#
# Letter changes algorithm: Replace every letter in the string with the letter following it in the alphabet.
# Then capitalize every vowel in this new string (a, e, i, o, u) and finally return this modified string.
#
# Author::    github.com/rozasdev (mailto:rozasdev at gmail.com)
# License::   Apache License 2.0

# Implements the changing algorithm.
def letter_changes(str)
  new_str = ""
  str.chars.each do |x|
    if x == 'z' || x == 'Z'
      new_str += 'a'
    elsif x =~ /[A-Ya-y]/
      new_str += (x.ord+1).chr
    else
      new_str += x
    end
  end
  new_str.chars.each_with_index do |x, i|
    if x =~ /[aeiou]/
      new_str[i] = x.upcase
    end
  end
  return new_str
end

# Just call the function.
puts letter_changes("fun times!")
puts letter_changes("hello*3")