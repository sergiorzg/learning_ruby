# encoding: UTF-8
#
# String reverse
#
# Author::    github.com/rozasdev (mailto:rozasdev at gmail.com)
# License::   Apache License 2.0

# Gets the reverse of a String
def reverse(str)
  str.reverse
end

# Just call the reverse function for an example String
puts reverse("This is a word")